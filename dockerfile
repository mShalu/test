FROM ubuntu

RUN apt update && apt install apache2 -y && apt install git -y

WORKDIR /var/www/html

RUN git clone https://github.com/srj261221/shalu_webpages.git

RUN cp -r shalu_webpages/enquiry-form/* .

EXPOSE 80

CMD ["apache2ctl", "-D", "FOREGROUND"]
